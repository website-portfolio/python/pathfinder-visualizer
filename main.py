import argparse
from typing import Dict, Type

from guis.base import BaseGui
from guis.gui_arcade import GuiArcade
from guis.gui_opencv import GuiCV2
from guis.gui_pygame import GuiPygame

parser = argparse.ArgumentParser()
parser.add_argument('mode', nargs='*', default='pygame',
                    help='Gui mode, to play choose from [arcade, cv2, pygame]. (default: pygame).')

if __name__ == '__main__':
    args = parser.parse_args()

    guis: Dict[str, Type[BaseGui]] = {
        'arcade': GuiArcade,
        'cv2': GuiCV2,
        'opencv': GuiCV2,
        'pygame': GuiPygame
    }

    game = guis[args.mode]()
    game.run()
