from enum import Enum


class GuiStates:
    pause = False
    solving = False
    solved = False
    running = True
    early_termination = False


class CellStates(Enum):
    Boundary = (0, 0, 0)  # Black
    Wall = (50, 50, 50)  # Black (can't be the same color as other values)
    Free = (200, 200, 200)  # Gray
    Path = (244, 212, 124)  # Yellow
    Active = (255, 0, 0)  # Red
    ToVisit = (32, 105, 224)  # Light blue
    Visited = (8, 44, 108)  # Dark blue
    Start = (255, 165, 0)  # Orange
    End = (0, 255, 0)  # Green


class GuiStatesCV2(GuiStates):
    # mouse
    left = False
    right = False
    count = 0

    running = False
