from queue import PriorityQueue
from typing import Callable, Union, TYPE_CHECKING

from core.cell import Cell
from core.states import CellStates

if TYPE_CHECKING:
    from core.gridworld import Grid2D


class Distance:
    modes = ['manhattan', 'euclidean', 'minkowski']

    def __init__(self, metric: str = 'manhattan', l_norm=3):
        self.metric = metric.lower()
        self.l_norm = l_norm

        if self.metric not in self.modes:
            raise ValueError(f"Unknown distance: {metric!r}, valid distances: {self.modes}")

    def distance(self, start: Cell, end: Cell) -> float:
        if self.metric == 'manhattan':
            return self.manhattan(start, end)
        if self.metric == 'euclidean':
            return self.manhattan(start, end)
        if self.metric == 'minkowski':
            return self.minkowski(start, end, self.l_norm)
        raise RuntimeError(f"Unhandled distance metric.")

    @staticmethod
    def manhattan(start: Cell, end: Cell) -> float:
        return Distance.minkowski(start, end, l=1)

    @staticmethod
    def euclidean(start: Cell, end: Cell) -> float:
        return Distance.minkowski(start, end, l=2)

    @staticmethod
    def minkowski(start: Cell, end: Cell, l=2) -> float:
        return sum(abs(a - b) ** l for a, b in zip(start.location, end.location)) ** 1 / l


class AStar:
    """
        A* pathfinding solver.
        
        :param Callable step_callback
            During the A* running this function will be called
            every step and can be used to process user interactions
            or  update the user on the progress. 
    """

    def __init__(self, step_callback: Callable, heuristic='euclidean', l_norm=3):
        self.step_callback = step_callback
        self.early_terminate = False
        self.distance = Distance(metric=heuristic, l_norm=l_norm).distance

    def init_scores(self, grid: 'Grid2D', start: Cell, end: Cell) -> tuple[dict[Cell, float], dict[Cell, float]]:
        f_score = {}
        g_score = {}
        for cell in grid:
            f_score[cell] = float('inf')
            g_score[cell] = float('inf')

        f_score[start] = self.distance(start, end)
        g_score[start] = 0
        return f_score, g_score

    def init_queues(self, start) -> tuple[int, PriorityQueue, dict[Cell, Union[None, Cell]]]:
        count = 0
        to_visit = PriorityQueue()
        to_visit.put((0, count, start))
        came_from = dict()
        came_from[start] = None
        return count, to_visit, came_from

    def solve(self, grid: 'Grid2D', start: Cell, end: Cell):
        count, to_visit, came_from = self.init_queues(start)
        f_scores, g_scores = self.init_scores(grid, start, end)

        to_visit_hash = {start}

        while not to_visit.empty():
            f_score, count, current = to_visit.get()
            to_visit_hash.remove(current)
            current.update_state(CellStates.Active)

            # Interact with the gui
            self.early_terminate = self.step_callback()
            if not self.early_terminate:
                if current == start:
                    current.update_state(CellStates.Start)
                if current == end:
                    current.update_state(CellStates.End)
                break

            if current == end:
                current.update_state(CellStates.End)
                break

            for neighbor in current.neighbors:
                temp_g_score = g_scores[current] + 1

                if temp_g_score < g_scores[neighbor]:
                    came_from[neighbor] = current
                    g_scores[neighbor] = temp_g_score
                    f_scores[neighbor] = temp_g_score + self.distance(neighbor, end)

                    if neighbor not in to_visit_hash:
                        count += 1
                        to_visit.put((f_scores[neighbor], count, neighbor))
                        to_visit_hash.add(neighbor)
                        neighbor.update_state(CellStates.ToVisit)

            if current == start:
                current.update_state(CellStates.Start)
            else:
                current.update_state(CellStates.Visited)

        self.color_path(came_from, end)

    def color_path(self, came_from, end):

        # No path was found.
        if end not in came_from:
            return

        current = came_from[end]
        while current in came_from:
            current, prev = came_from[current], current
            prev.update_state(CellStates.Path)
            if current is not None:
                current.update_state(CellStates.Active)
            else:
                prev.update_state(CellStates.Start)
            self.step_callback()
