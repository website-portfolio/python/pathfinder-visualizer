from enum import Enum

from core.algorithms.astar import AStar


class Algorithms(Enum):
    a_star = AStar
