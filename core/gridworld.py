import numpy as np

from typing import Callable
from collections import namedtuple

from core.cell import Cell
from core.states import CellStates
from core.algorithms.collection import Algorithms


class Grid2D:
    size_ = namedtuple('size', ('width', 'height'))

    def __init__(self, world_size: tuple[int, int], square_size: tuple[int, int]):
        self.world_size = self.size_(*world_size)
        self.square_size = self.size_(*square_size)
        self.grid = self.create_grid(self.world_size, self.square_size)

    def __getitem__(self, item):
        return self.grid[item]

    def __len__(self):
        return self.grid.size

    def __iter__(self):
        for row in self.grid:
            for cell in row:
                yield cell

    @property
    def get_start(self) -> Cell:
        start = self.get_state(CellStates.Start)
        return start[0] if start else None

    @property
    def get_end(self) -> Cell:
        start = self.get_state(CellStates.End)
        return start[0] if start else None

    def get_state(self, state: CellStates) -> list[Cell]:
        return [cell for cell in self if cell.state == state]

    def create_grid(self, world_size: tuple[int, int], square_size: tuple[int, int]) -> 'np.typename[Cell]':
        """ Generate the grid, where the boundary cells are marked.  """

        # Initialize the grid with free cells.
        grid = np.empty(world_size, dtype=np.object)
        for row in range(world_size.height):
            for column in range(world_size.width):
                cell = Cell(row, column, square_size.width, square_size.height, CellStates.Boundary)
                grid[row, column] = cell

        # fix boundary values
        for row in grid[1:-1, 1:-1]:
            for cell in row:
                cell.state = CellStates.Free

        # Update all neighbor values
        for row in grid:
            for cell in row:
                cell.update_neighbors(grid)

        return grid

    def update_cells(self, restricted=(CellStates.Boundary, CellStates.Wall)):
        """ Update all neighboring cells, should be called before solving.  """
        for cell in self:
            cell.update_neighbors(self.grid, restricted)

    def reset(self):
        """ Reset the grid to the start position.  """
        self.grid = self.create_grid(self.world_size, self.square_size)

    def solve(self, algorithm: str, step_callback: Callable):
        algorithm = Algorithms[algorithm].value(step_callback)
        algorithm.solve(self, self.get_start, self.get_end)

    def undo_solving(self):
        for state in [CellStates.ToVisit, CellStates.Visited, CellStates.Active, CellStates.Path]:
            for cell in self.get_state(state):
                cell.update_state(CellStates.Free)
