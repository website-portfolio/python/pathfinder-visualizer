from dataclasses import dataclass, field

from core.states import CellStates


@dataclass(unsafe_hash=True)
class Cell:
    """
    Helper class, that maintains the state of a single cell.

    :param int row
        The row on which this cell is located

    :param int columns
        The column on which this cell is located.

    :param int width
        The height and width of the square.

    :param States color
        The state and color of the current cell.

    """

    row: int
    column: int
    width: int = field(hash=False)
    height: int = field(hash=False)
    state: CellStates = field(hash=False, default=CellStates.Boundary)
    changed: bool = field(hash=False, default=True)
    neighbors: list[tuple[int, int]] = field(default_factory=list, repr=False, hash=False)

    @property
    def location(self) -> tuple[int, int]:
        """ Return the grid location. """
        return self.row, self.column

    @property
    def coordinates(self) -> tuple[int, int]:
        """ Return the image location (left top).  """
        return self.row * self.height, self.column * self.width

    @property
    def bbox(self) -> tuple[int, int, int, int]:
        row, column = self.coordinates
        return (row, column, self.width, self.height,)

    @property
    def center(self) -> tuple[int, int]:
        """ Return coordinate centers"""
        row, column = self.coordinates
        return row + self.height // 2, column + self.width // 2

    @property
    def nr_neighbors(self):
        return len(self.neighbors)

    @staticmethod
    def in_bounds(location: tuple[int, int], nr_rows: int, nr_columns: int) -> bool:
        """ Return True if a location is inside the grid boundaries. """
        row, columns = location
        if row < 1 or row > nr_rows - 2:
            return False
        if columns < 1 or columns > nr_columns - 2:
            return False
        return True

    def update_neighbors(self, grid, restricted=(CellStates.Boundary, CellStates.Wall)):
        """ Add direct neighbors, that are on the grid and are not restricted.  """

        self.neighbors = []
        if self.state in restricted:
            return self.neighbors

        nr_rows, nr_columns = grid.shape[:2]
        for dir_row, dir_columns in [(0, -1), (0, 1), (-1, 0), (1, 0)]:
            new_row, new_col = self.row + dir_row, self.column + dir_columns
            if self.in_bounds((new_row, new_col), nr_rows, nr_columns):
                if grid[new_row, new_col].state not in restricted:
                    self.neighbors.append(grid[new_row, new_col])
        return self.neighbors

    def update_state(self, state):
        """ All changes to cell states can be caught here.  """
        self.state = state
        self.changed = True

    def __lt__(self, other):
        return False
