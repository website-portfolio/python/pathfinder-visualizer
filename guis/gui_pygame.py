import logging
import pygame

from core.cell import Cell
from core.states import CellStates
from guis.base import BaseGui


class GuiPygame(BaseGui):
    caption = "Pathfinding Pygame"

    def __init__(self, world_size: tuple[int, int] = (50, 50), screen_size: tuple[int, int] = (800, 800)):
        super().__init__(world_size, screen_size)
        self.window = pygame.display.set_mode(self.screen_size)
        pygame.display.set_caption(self.caption)

    def run(self):
        """ Running the main game loop.  """
        while self.states.running:
            self.on_draw()
        pygame.quit()

    def on_step_callback(self):
        self.on_draw()
        return not self.states.early_termination

    def on_draw(self, *_):
        self.draw_cells()
        self.draw_grid()
        self.handle_events()
        while self.states.pause:
            self.handle_events()
        pygame.display.update()

    def draw_cells(self):
        for cell in self.grid:
            if cell.changed:
                pygame.draw.rect(self.window, cell.state.value, cell.bbox)

    def draw_grid(self):
        grid_width = self.grid.square_size.width * self.grid.world_size.width
        square_height = self.grid.square_size.height
        for i in range(self.grid.world_size.height):
            pygame.draw.line(self.window, CellStates.Boundary.value,
                             (0, i * square_height), (grid_width, i * square_height), 1)

        grid_height = self.grid.square_size.height * self.grid.world_size.height
        square_width = self.grid.square_size.width
        for j in range(self.grid.world_size.width):
            pygame.draw.line(self.window, CellStates.Boundary.value,
                             (j * square_width, 0), (j * square_height, grid_height), 1)

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.states.running = False

            if event.type == pygame.KEYDOWN:
                self.handle_keyboard(event)

            # Only update the board when not solving.
            if not self.states.solving:
                if pygame.mouse.get_pressed(num_buttons=3)[0]:  # left
                    self.handle_left_mouse_click()

                if pygame.mouse.get_pressed(num_buttons=3)[2]:  # right
                    self.handle_right_mouse_click()

    def handle_left_mouse_click(self):
        logging.debug(f"Left mouse click registered: {pygame.mouse.get_pos()}")
        row, column = self.get_clicked_row(pygame.mouse.get_pos())
        cell: Cell = self.grid[row, column]
        self.place_cells(cell)

    def handle_right_mouse_click(self):
        logging.debug(f"Right mouse click registered: {pygame.mouse.get_pos()}")
        row, column = self.get_clicked_row(pygame.mouse.get_pos())
        cell: Cell = self.grid[row, column]
        self.remove_cells(cell)

    def handle_keyboard(self, event):
        logging.debug(f"Keyboard event registered: {event}")

        # quit
        if event.key in [pygame.K_q, pygame.K_ESCAPE]:
            self.states.early_termination = True
            self.states.running = False

        if event.key == pygame.K_p:
            self.states.pause = not self.states.pause

        if event.key == pygame.K_t and self.states.solving:
            self.states.early_termination = True

        # Only allow to reset and start solving, when not yet solving.
        if not self.states.solving:

            # Create a new game
            if event.key == pygame.K_n:
                self.reset()

            # begin solving
            valid_start_end = self.start is not None and self.end is not None
            if event.key == pygame.K_SPACE and valid_start_end and not self.states.solved:
                self.solve(step_callback=self.on_step_callback)

            # Reset the game to be solved again
            if event.key == pygame.K_r and self.states.solved:
                self.undo()


if __name__ == '__main__':
    gui = GuiPygame()
    gui.run()
