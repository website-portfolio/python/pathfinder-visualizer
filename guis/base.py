from collections import namedtuple

from core.cell import Cell
from core.gridworld import Grid2D
from core.states import CellStates, GuiStates


class BaseGui:
    size_ = namedtuple('size', ('width', 'height'))
    caption = "Pathfinding opencv"

    def __init__(self, world_size: tuple[int, int] = (50, 50), screen_size: tuple[int, int] = (800, 800)):
        self.world_size = self.size_(*world_size)
        self.screen_size = self.size_(*screen_size)
        self.square_size = self.size_(*tuple(int(a // b) for a, b in zip(self.screen_size, self.world_size)))
        # refit screen size, to avoid pixels sticking out.
        self.screen_size = self.size_(*tuple(int(a * b) for a, b in zip(self.square_size, self.world_size)))

        # noinspection PyTypeChecker
        self.grid = Grid2D(world_size=world_size, square_size=self.square_size)
        self.states = GuiStates

        self.start = None
        self.end = None

    def get_clicked_row(self, position):
        """ Converts the image coordinates to grid coordinates. """
        row, column = tuple(map(lambda x: int(x[0] / x[1]), zip(position, self.square_size)))
        if Cell.in_bounds((row, column), *self.world_size):
            return row, column  # Drawing from left top.
        return 0, 0

    def place_cells(self, cell):
        if not self.states.solving:
            if self.start is None and cell.location != self.end and cell.state != CellStates.Boundary:
                self.start = cell.location
                cell.update_state(CellStates.Start)

            elif self.end is None and cell.location != self.start and cell.state != CellStates.Boundary:
                self.end = cell.location
                cell.update_state(CellStates.End)

            elif self.end is not None and self.start is not None and cell.state == CellStates.Free:
                cell.update_state(CellStates.Wall)
        return cell

    def remove_cells(self, cell):
        if not self.states.solving:
            if cell.location == self.start:
                self.start = None
                cell.update_state(CellStates.Free)

            elif cell.location == self.end:
                self.end = None
                cell.update_state(CellStates.Free)

            elif cell.state == CellStates.Wall:
                cell.update_state(CellStates.Free)
        return cell

    def undo(self):
        self.states.solved = False
        self.grid.undo_solving()

    def reset(self):
        self.grid.reset()
        self.start = self.end = None
        self.states.solved = False

    def solve(self, step_callback):
        self.states.solving = True
        self.grid.update_cells()
        self.grid.solve(algorithm='a_star', step_callback=step_callback)
        self.states.solving = False
        self.states.solved = True
        self.states.early_termination = False

    def run(self):
        raise NotImplementedError

    def on_step_callback(self):
        raise NotImplementedError

    def on_draw(self):
        raise NotImplementedError

    def draw_cells(self):
        raise NotImplementedError

    def draw_grid(self):
        raise NotImplementedError
