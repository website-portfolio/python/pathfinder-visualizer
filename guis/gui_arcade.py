import arcade
import logging
import numpy as np
import os

from PIL import Image
from pathlib import Path

from core.cell import Cell
from core.states import CellStates
from guis.base import BaseGui

PATH_SPRITES = os.path.join(str(Path(__file__).parents[0]), 'sprites')


class ArcadeCell(arcade.sprite.Sprite):

    def __init__(self, cell: Cell):
        self.cell = cell
        super().__init__(self.filename)
        self.set_position(*self.cell.center)

    def __getattr__(self, item):
        return getattr(self.cell, item)

    def __repr__(self):
        return f"<{type(self).__name__} (cell={self.cell})"

    @property
    def filename(self):
        return os.path.join(PATH_SPRITES, f"{self.cell.state.name}.png")

    def update_state(self):
        if self.cell.changed:
            return ArcadeCell(cell=self.cell)
        return self


class GuiArcade(arcade.Window, BaseGui):
    caption = "Pathfinding Arcade"

    def __init__(self, world_size: tuple[int, int] = (50, 50), screen_size: tuple[int, int] = (800, 800)):
        BaseGui.__init__(self, world_size, screen_size)
        arcade.Window.__init__(self, self.screen_size.width, self.screen_size.height, title=self.caption)
        self.set_update_rate(1)

        # Generate sprite images and sprite list
        self.sprites = arcade.SpriteList(is_static=True)
        self._generate_sprites()
        self._create_sprites()

    # Extra helpers to speed up arcade using sprite lists.
    def _generate_sprites(self):
        for cell_state in CellStates:
            image = Image.fromarray(np.full((*self.square_size, 3), fill_value=cell_state.value, dtype=np.uint8))
            image.save(os.path.join(PATH_SPRITES, f"{cell_state.name}.png"))

    def _create_sprites(self):
        for cell in self.grid:
            self.sprites.append(ArcadeCell(cell=cell))

    # Common methods
    def run(self):
        arcade.run()

    def on_step_callback(self):
        self.update_sprites()
        self.on_draw()
        self.dispatch_events()
        return not self.states.early_termination

    def update_sprites(self):
        for index in range(len(self.sprites)):
            self.sprites[index] = self.sprites[index].update_state()

    def on_draw(self):
        if not self.states.running:
            arcade.close_window()
            exit(0)

        arcade.start_render()
        self.sprites.draw()
        self.draw_grid()
        arcade.finish_render()

    def draw_grid(self):
        grid_width = self.grid.square_size.width * self.grid.world_size.width
        square_height = self.grid.square_size.height
        for i in range(self.grid.world_size.height):
            arcade.draw_line(0, i * square_height, grid_width, i * square_height, CellStates.Boundary.value)

        grid_height = self.grid.square_size.height * self.grid.world_size.height
        square_width = self.grid.square_size.width
        for j in range(self.grid.world_size.width):
            arcade.draw_line(j * square_width, 0, j * square_height, grid_height, CellStates.Boundary.value)

    def on_mouse_press(self, x: float, y: float, button: int, modifiers: int):
        if button == arcade.MOUSE_BUTTON_LEFT:
            logging.debug(f"Left mouse click {x}, {y}, {modifiers}")
            self.handle_left_mouse_click(x, y)

        if button == arcade.MOUSE_BUTTON_RIGHT:
            logging.debug(f"Right mouse click {x}, {y}, {modifiers}")
            self.handle_right_mouse_click(x, y)

    def handle_left_mouse_click(self, x, y):
        row, column = self.get_clicked_row((x, y))
        cell: Cell = self.grid[row, column]
        self.place_cells(cell)
        self.update_texture(cell)

    def handle_right_mouse_click(self, x, y):
        row, column = self.get_clicked_row((x, y))
        cell: Cell = self.grid[row, column]
        self.remove_cells(cell)
        self.update_texture(cell)

    def update_texture(self, cell):
        for index in range(len(self.sprites)):
            if self.sprites[index].cell == cell:
                self.sprites[index] = ArcadeCell(cell)

    def update_state(self, cell: Cell, state: CellStates):
        cell.update_state(state)
        self.update_texture(cell)

    def on_mouse_drag(self, x: float, y: float, dx: float, dy: float, buttons: int, modifiers: int):
        row, column = self.get_clicked_row((x, y))
        cell: Cell = self.grid[row, column]

        if not self.states.solving:
            if buttons == arcade.MOUSE_BUTTON_LEFT:
                logging.debug(f"Dragging left, {x}, {y}")
                if self.end is not None and self.start is not None and cell.state == CellStates.Free:
                    self.update_state(cell, CellStates.Wall)

            if buttons == arcade.MOUSE_BUTTON_RIGHT:
                logging.debug(f"Dragging right, {x}, {y}")
                if cell.state == CellStates.Wall:
                    self.update_state(cell, CellStates.Free)

    def on_key_press(self, symbol: int, modifiers: int):
        logging.debug(f"Keyboard event registered: {symbol}, modifiers: {modifiers}")
        if symbol in [arcade.key.Q, arcade.key.ESCAPE]:
            self.states.early_termination = True
            self.states.running = False

        if symbol == arcade.key.P:
            self.states.pause = not self.states.pause

        if symbol == arcade.key.T and self.states.solving:
            self.states.early_termination = True

        # Only allow to reset and start solving, when not yet solving.
        if not self.states.solving:

            # Create a new game
            if symbol == arcade.key.N and not self.states.solving:
                self.reset()
                self.update_sprites()

            # begin solving
            valid_start_end = self.start is not None and self.end is not None
            if symbol == arcade.key.SPACE and valid_start_end and not self.states.solved:
                self.solve(step_callback=self.on_step_callback)
                self.update_sprites()

            # Reset the game to be solved again
            if symbol == arcade.key.R and self.states.solved:
                self.undo()
                self.update_sprites()


if __name__ == '__main__':
    gui = GuiArcade()
    gui.run()
