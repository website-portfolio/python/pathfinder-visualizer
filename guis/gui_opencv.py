import logging
import numpy as np
import cv2

from core.cell import Cell
from core.states import CellStates, GuiStatesCV2
from guis.base import BaseGui


class GuiCV2(BaseGui):
    caption = "Pathfinding opencv"

    def __init__(self, world_size: tuple[int, int] = (50, 50), screen_size: tuple[int, int] = (800, 800)):
        BaseGui.__init__(self, world_size, screen_size)
        self.image = np.zeros((*self.screen_size, 3), dtype=np.uint8)
        self.states = GuiStatesCV2

    def run(self):
        """ Running the main game loop.  """

        # Setup to make sure that mouse handling is done correctly.
        self.on_draw(1)
        cv2.setMouseCallback(self.caption, self.handle_mouse)
        self.states.running = True

        while self.states.running:
            self.on_draw(delay=1)
        cv2.destroyAllWindows()

    def on_step_callback(self):
        self.on_draw()
        return not self.states.early_termination

    def on_draw(self, delay=1):
        while self.states.pause:
            self.handle_keyboard(1000)

        self.draw_cells()
        self.draw_grid()

        if self.states.running and cv2.getWindowProperty(self.caption, 0) == -1:
            self.states.running = False
        else:
            cv2.imshow(self.caption, cv2.cvtColor(self.image.copy(), cv2.COLOR_RGB2BGR))
            self.handle_keyboard(delay)

    def draw_cells(self):
        for cell in self.grid:
            if cell.changed:
                row, column = cell.coordinates
                self.image[column:column + cell.width, row: row + cell.height] = cell.state.value
                cell.changed = False

    def draw_grid(self):
        self.image[::self.square_size.height, :] = CellStates.Boundary.value
        self.image[:, ::self.square_size.width] = CellStates.Boundary.value

    def handle_mouse(self, event, x, y, flags, param):
        logging.debug(f"\nHandling mouse: {event}, {x}, {y}, {flags}, {param}")

        # Left press = 1, left up = 4
        if event == 1:
            self.states.left = True
            self.handle_left_mouse_click(x, y)

        if event == 4:
            self.states.left = False
            self.states.count -= 1

        # Right press = 2, right up = 5
        if event == 2:
            self.states.right = True if event == 2 else False
            self.handle_right_mouse_click(x, y)

        if event == 5:
            self.states.right = False

        if event == 0:
            if self.states.left:
                self.handle_left_mouse_click(x, y)
            if self.states.right:
                self.handle_right_mouse_click(x, y)

    def handle_left_mouse_click(self, x, y):
        logging.debug(f"\tLeft mouse click registered: {x}, {y}")
        row, column = self.get_clicked_row((x, y))
        cell: Cell = self.grid[row, column]
        self.place_cells(cell)

    def handle_right_mouse_click(self, x, y):
        logging.debug(f"\tRight mouse click registered: {x}, {y}")
        row, column = self.get_clicked_row((x, y))
        cell: Cell = self.grid[row, column]
        self.remove_cells(cell)

    def handle_keyboard(self, delay):
        key = cv2.waitKey(delay)
        if key != -1:
            logging.debug(f"Handling keyboard inputs, {key}")

            if key in [ord('q'), 27]:  # 27 = Escape
                self.states.early_termination = True
                self.states.running = False

            if key == ord('p'):
                self.states.pause = not self.states.pause

            if key == ord('t') and self.states.solving:
                self.states.early_termination = True

            if not self.states.solving:
                # Create a new game
                if key == ord('n'):
                    self.reset()

                # begin solving (key 32 = Space)
                valid_start_end = self.start is not None and self.end is not None
                if key == 32 and valid_start_end and not self.states.solved:
                    self.solve(step_callback=self.on_step_callback)

                # Reset the game to be solved again
                if key == ord('r'):
                    self.undo()


if __name__ == '__main__':
    gui = GuiCV2()
    gui.run()
