# Pathfinding visualizers

A visualizer is a good tool to bring the algorithms from the backend to the front end. In this repository the same A*
pathfinder visualizer is made, but using different languages. For some languages, such as python, different frontend
packages were used. To represent the visualization and interaction. When different frontend packages were used, the
backend will be the same for all packages, therefore there is minimal code duplication.

## Interaction:

All guis in this project share the same interaction set and have the same similar gui view. On run a grid will be shown
of `50`x`50` cells/nodes, on a screen of  `800`x`800` pixels. The screen exists of out `boundary` cells (the `black`
border), which cannot be removed or edited and `free` cells (the `white` ones).

The mouse will be used for creating the `start`, `end` and `walls` for the pathfinding algorithm. While the keyboard
keys can be used to `start`, `pause`, `reset` and `terminate` the gui. Read the [keyboard](#keyboard) section for all
the key combinations and meanings.

## Setup Pycharm

- Create a virtual environment

  ```cmd
  cd python
  pip install pipenv
  pipenv shell
  ```

## Running the projects

```cmd
python -m main pygame
```

Where `pygame`, can be replaced by `arcade`, `opencv` or `cv2`. 
Alternatively you can call the respective gui directly by running

<center>

| Gui                 | File                 | 
|---------------------|----------------------|
| Pygame              | `guis/gui_pygame.py` |
| Arcade              | `guis/gui_arcade.py` |
| opencv-python (cv2) | `guis/gui_opencv.py` |

</center>

## Commands

The following keys and interactions work for all guis in the same way:


### Mouse

- Left Button 
  1) If there is no `start` cell, it will be placed at the mouse location.
  2) If there is a `start` cell, and no `end` cell, the `end` cell will be placed.
  3) If there is a `start` and `end` cell, a `wall` (black square) will be placed. 
    You can click and hold (drag) the mouse to generate walls continuously.
    
<br>

- Right Button
  1) Make the current cell `free` again, independent if it was a `start`, `end` or `wall` cells. Only the `boundary`
    cells will remain the same.
  2) When dragging, will `free` all cells under the mouse position.

### Keyboard

- <kbd>SPACE</kbd>: start generating a path between the start and end point.
- <kbd>P</kbd>: pause the game when running (no indication provided, whoops)
- <kbd>T</kbd>: terminate solving the game (pausing, doesn't influence solving state.)
- <kbd>R</kbd>: remove the drawn pad, but maintain the start, end and walls.
- <kbd>N</kbd>: empty the entire grid.
- <kbd>Q</kbd> or  <kbd>ESC</kbd>: quit the guis. 
  

